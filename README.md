# Compiling linphone for Ubuntu Touch

In order to run linphone on an Ubuntu Touch device it needs to be specifically
compiled for them. This is due to the codecs needing specific compiler directives
for the arm platform (the debs in apt work, but not well enough to make a call).
Additionally ortp has a small tweak to fix some issues with click packages and
their confinement. Linphone also has some tweaks to the build process and some
segfault fixes.

## Building

Building is can be done on a desktop with [clickable](https://github.com/bhdouglass/clickable)
setup and working.

- Clone this repo: `git clone https://gitlab.com/ubports-linphone/compile-linphone-ubuntu-touch.git`
- Initialize submodules: `git submodule update --init --recursive`
- Compile linphone and dependencies: `./build.sh`

## Testing

- Copy binaries and libraries to your phone: `adb push ./output /home/phablet/linphone/`
- Connect to the phone: `clickable shell`
- Run linphonec: `LD_LIBRARY_PATH=/home/phablet/linphone/lib/arm-linux-gnueabihf linphone/bin/linphonec`

## Building the Docker Image

In the docker directory run `docker build -t linphone-compile .`. The docker
image is based off the image from [clickable](http://clickable.bhdouglass.com/en/latest/).
